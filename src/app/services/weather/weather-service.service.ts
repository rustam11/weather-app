import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherServiceService {
  openWeatherKey = environment.openWeatherMapApiKey;

  constructor(private http: HttpClient) {
  }

  currentLocationWeather(lat: number, lng: number) {
    const url = `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&APPID=${this.openWeatherKey}`;
    return this.http.get(url);
  }

  searchForForecast(searchTerm: string) {
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${searchTerm}&APPID=${this.openWeatherKey}`;
    return this.http.get(url).pipe(catchError(error =>  {
      throw new Error('error in source. Details: ' + error);
    }));
  }
}
