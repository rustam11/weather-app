import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatButtonToggleChange} from '@angular/material/button-toggle';
import {NgForm} from '@angular/forms';

import {WeatherServiceService} from '../../services/weather/weather-service.service';
import {GeolocationService} from '../../services/geolocation/geolocation.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit, OnDestroy {
  celsiusMode = true;
  isLoading = false;
  currentPosition: any;
  tempInCels: number = null;
  tempInFar: number = null;
  currentTempMode: number = null;
  windDirection: string;
  localForecast = true;
  showSearch = false;
  searchSub: Subscription;
  currentLocationSub: Subscription;

  constructor(
    private weathService: WeatherServiceService,
    private geoService: GeolocationService
  ) {
  }

  ngOnInit() {
    this.getCurrentPosition();
  }

  getCurrentPosition() {
    this.isLoading = true;
    this.showSearch = false;
    this.geoService.getPosition().then(pos => {
      this.currentLocationSub = this.weathService.currentLocationWeather(pos.lat, pos.lng).subscribe(result => {
        this.currentPosition = result;
        this.turnFarIntoCel();
        this.currentTempMode = this.tempInCels;
        this.windDirection = this.degToCompass(this.currentPosition.wind.deg);
        this.localForecast = true;
        this.isLoading = false;
      }, err => {
        this.isLoading = false;
        this.currentPosition = null;
      });
    });
  }

  changeCity() {
    this.localForecast = !this.localForecast;
    this.showSearch = !this.showSearch;
  }

  searchForForecast(f: NgForm) {
    this.isLoading = true;
    this.searchSub = this.weathService.searchForForecast(f.value.searchTerm.toLowerCase()).subscribe(res => {
        this.currentPosition = res;
        this.turnFarIntoCel();
        this.currentTempMode = this.tempInCels;
        this.windDirection = this.degToCompass(this.currentPosition.wind.deg);
        this.isLoading = false;
      }, err => {
        this.isLoading = false;
        this.currentPosition = null;
      }
    );
  }

  degToCompass(num) {
    const val = Math.floor((num / 22.5) + 0.5);
    const arr = [
      'north', 'orth-northeast',
      'northeast ', 'east-northeast',
      'east', 'east-southeast',
      'southeast', 'south-southeast',
      'south', 'south-southwest',
      'southwest ', 'west-southwest',
      'west', 'west-northwest',
      'northwest ', 'north-northwest'
    ];
    return arr[(val % 16)];
  }

  turnFarIntoCel() {
    this.tempInCels = Math.floor(this.currentPosition.main.temp - 273.15);
    this.tempInFar = Math.floor(this.currentPosition.main.temp * 9 / 5 - 459.67);
  }

  changeOutputTemp(ev: MatButtonToggleChange) {
    if (ev.value === 'celsius') {
      this.celsiusMode = true;
      this.currentTempMode = this.tempInCels;
    } else {
      this.celsiusMode = false;
      this.currentTempMode = this.tempInFar;
    }
  }

  ngOnDestroy(): void {
    if (this.searchSub) {
      this.searchSub.unsubscribe();
    }
    if (this.currentLocationSub) {
      this.currentLocationSub.unsubscribe();
    }
  }
}
